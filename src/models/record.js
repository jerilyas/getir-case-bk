const mongoose = require('mongoose');

const recordSchema = mongoose.Schema({
  _id: mongoose.Types.ObjectId,
  key: String,
  counts: [],
  value: String
},
  {
    timestamps: { createdAt: true, updatedAt: false }
  });

const Record = mongoose.model('record', recordSchema);
module.exports = Record;