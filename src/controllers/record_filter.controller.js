const { validationResult } = require('express-validator');
const Record = require('../models/record');
const recordsFilterService = require('../services/record_filter.service');
const httpStatus = require('http-status');

 
module.exports = pf = async (req, res, next) => {
    // Gets validation results from record filter validation
    const errors = validationResult(req);
    // If any fail occured in resource validation it returns all the validation errors with concatenated error messages
    if (!errors.isEmpty()) {
        return res.status(400).json({
            code: httpStatus.BAD_REQUEST, msg: errors.array().map(function (item) {
                return item.msg
            }).join(' ')
        });
    }
    const { minCount, maxCount, startDate, endDate } = req.body;
    // filterRecords function from creates and aggregated query and results are passed to callback
    const records = await Record.aggregate(recordsFilterService.filterRecords(minCount, maxCount, startDate, endDate)).then(function (records) {
        // Returns filtered records for given criteria          
        return res.status(httpStatus.OK).json({ code: 0, msg: "Success", "records": records });
    })
        .catch(function (err) {
            // Returns mongodb errorcode and error name
            return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({ code: err.code, msg: err.name });
        });
    next();
}
