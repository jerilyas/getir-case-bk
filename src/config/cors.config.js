const cors = require('cors');

const options = {
	origin: (origin, callback) => {
		// In dev, allow these origins to access the API
		const whiteList = ['localhost', 'chrome-extension', '*'];
		// We are doing string matching here.
		const index = whiteList.findIndex((aWhiteListedOrigin) => origin.includes(aWhiteListedOrigin));
        
        callback(null, true);
	},
	credentials: false,
};

module.exports = () => cors(options);