
const express = require('express');
const cors = require('./cors.config');
const routes = require('../routes');
const app = express();

// This middleware take care of the origin when the origin is undefined.
// origin is undefined when request is local
app.use((req, _, next) => {
	req.headers.origin = req.headers.origin || req.headers.host;
	next();
});

// req.body Configuration
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// CORS configuration
app.use(cors());

// Mount routes
app.use(routes);


module.exports = app;