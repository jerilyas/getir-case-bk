module.exports = sv = {
       // Creates aggregated query to filter "sum of count array" for specified daterange
       filterRecords: function (minCount, maxCount, startDate, endDate) {
        return [{
            $project: {
                "createdAt": 1,
                "key": 1,
                "_id": 0,
                "totalCount": {
                    $sum: "$counts",
                }
            }
        },
        {
            $match:
            {
                $and: [
                    { totalCount: { $gte: minCount, $lte: maxCount } },
                    { createdAt: { $gte: new Date(startDate), $lte: new Date(endDate) } },
                ]
            }
        }]
    }
};