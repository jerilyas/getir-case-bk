const express = require('express');
var swaggerUi = require('swagger-ui-express');
var swaggerDocument = require('../../swagger.json');
const authorization = require('../middlewares/authorization');
const httpStatus = require('http-status');

// import all the routes here
const recordRoutes = require('./records.js');

const router = express.Router();
// Swagger Documentation Init
router.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// ApiKey Authorization Middleware
router.use(authorization);

// Routing - Record operations
router.use('/records', recordRoutes);


// 404 Routing
router.get('*', function(req, res){
    res.status(httpStatus.NOT_FOUND).json({ code: httpStatus.NOT_FOUND, msg: httpStatus['404_NAME'] });
});

module.exports = router;