const express = require('express');
const validation = require('../validations/record_filter.validation');
const controller = require('../controllers/record_filter.controller');

const router = express.Router();

// This module uses validation and controller
router.post('/',validation.recordFilterValidation, controller);
module.exports = router;