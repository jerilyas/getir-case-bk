const { check } = require('express-validator');


module.exports = {
    // Route input resource validation for filterRecords 
    recordFilterValidation:
        [check('minCount').exists().withMessage('minCount cannot be empty.').isInt().withMessage('minCount should be an integer.'),
        check('maxCount').exists().withMessage('maxCount cannot be empty.').isInt().withMessage('maxCount should be an integer.'),
        check('startDate').exists().withMessage('startDate cannot be empty.').isDate().withMessage('startDate should be date.'),
        check('endDate').exists().withMessage('endDate cannot be empty.').isDate().withMessage('endDate should be an date.')]
};