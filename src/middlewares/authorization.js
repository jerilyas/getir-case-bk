const apiKeys = require('../../etc/api_keys');
const httpStatus = require('http-status');

module.exports = (req, res, next) => {
    const apiKey = req.get('X-API-KEY');
    if (apiKeys.has(apiKey)) {
        next();
    } else {
        // 401 or 403 (Unauthorized/Forbidden)
        return res.status(httpStatus.UNAUTHORIZED).json({ code: 403, msg: httpStatus['401_NAME'] });
    }
};