const recordsFilterService =  require('../../src/services/record_filter.service');
const config =  require('../../config');
const mongoose = require('mongoose');

beforeAll(async () => {
    // Connect to a Mongo DB
    await mongoose.connect(config.MONGODB_URI, { useNewUrlParser: true, useUnifiedTopology: true })
});

afterAll(async () => {
    // Closes the Mongoose connection
    await mongoose.connection.close()
});

 describe('api', () => {
    describe('post /records', () => {
        test('should get db error for malformed aggregated query', async () => {
            const Record = require('../../src/models/record');
            await Record.aggregate(recordsFilterService.filterRecords(1, 0, '2010-01-01', '2010-01-01')).then(function (records) {
                // Returns filtered records for given criteria          
                expect(records.length).toBe(0)
            }).catch(function (err) {
                fail('if Db error occures it should fail');
            });
        });
    });
});  