const request = require('supertest');
const app = require('../../src/config/express.config');
const config =  require('../../config');
const httpStatus = require('http-status');

describe('api', () => {
    describe('auth', () => {
        test('should return unauthorized', async () => {
            const response = await request(app).post('/records')
            .set({ Accept: 'application/json' }).then((response) => {
                expect(response.status).toBe(httpStatus.UNAUTHORIZED);
            });
        });
    });
}); 