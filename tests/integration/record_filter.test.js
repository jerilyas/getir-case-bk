const request = require('supertest');
const app = require('../../src/config/express.config');
const mongoose = require('mongoose');
const config = require('../../config');
const httpStatus = require('http-status');
const apiKeys = require('../../etc/api_keys');

beforeAll(async () => {
    // Connect to a Mongo DB
    await mongoose.connect(config.MONGODB_URI, { useNewUrlParser: true, useUnifiedTopology: true })
});

afterAll(async () => {
    // Closes the Mongoose connection
    await mongoose.connection.close()
});

describe('api', () => {
    const testApiKey = apiKeys.keys().next().value;
    jest.setTimeout(15000);
    describe('post /records', () => {
        test('should return a 200', async () => {
            const postData = {
                minCount: 0,
                maxCount: 0,
                startDate: '2010-01-01',
                endDate: '2010-01-01',
            };
            const response = await request(app).post('/records').send(postData)
                .set({ 'X-API-KEY': testApiKey, Accept: 'application/json' }).then((response) => {
                    expect(response.status).toBe(httpStatus.OK);
                });
        });
        test('should return a 200 with empty result set', async () => {
            const postData = {
                minCount: 1,
                maxCount: 0,
                startDate: '2010-01-01',
                endDate: '2010-01-01',
            };
            const response = await request(app).post('/records').send(postData)
                .set({ 'X-API-KEY': testApiKey, Accept: 'application/json' }).then((response) => {
                    expect(response.status).toBe(httpStatus.OK);
                    expect(response.body.records.length).toBe(0);
                });
        });
        test('should return a 400 with code 400 invalid post data', async () => {
            const postData = {
                startDate: '2010-01-01',
                endDate: '2010-01-01',
            };
            const response = await request(app).post('/records').send(postData)
                .set({ 'X-API-KEY': testApiKey, Accept: 'application/json' }).then((response) => {
                    expect(response.status).toBe(httpStatus.BAD_REQUEST);
                    expect(response.body.code).toBe(400);
                });
        });
    });
});