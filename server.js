const config = require('./config');
const db = require('./src/config/mongoose.config');
const app = require('./src/config/express.config');

app.listen(config.PORT, (err) => {
    if(err) {
		return console.log('server failed to start', err);
	}
    return console.log(`Server started on port ${config.PORT}`);
})


module.exports = app;